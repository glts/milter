# milter changelog

## 0.2.4 (2021-12-06)

*   Several minor improvements in the documentation and examples, and updates to
    project metadata.
*   Update dependencies in `Cargo.lock`.

## 0.2.3 (2021-05-03)

*   Properly specify minimal dependency versions in `Cargo.toml`.
*   Update dependencies in `Cargo.lock`.

## 0.2.2 (2021-01-07)

*   Update example milter to use dependency `once_cell` and drop dev dependency
    on `lazy_static`.
*   Update dependencies in `Cargo.lock`.
*   Various minor improvements in project metadata, examples, and documentation.

## 0.2.1 (2020-05-17)

*   Add builder option `Milter::remove_socket` (`smfi_opensocket(rmsocket)` in
    libmilter).
*   Note minimum supported Rust version 1.42.0 in README.
*   Revise doc strings.

## 0.2.0 (2020-02-09)

Initial release.
