mod common;

use milter::*;

#[test]
fn add_header() {
    let test_name = common::test_name(file!());
    let miltertest = common::spawn_miltertest_runner(file!());

    Milter::new("inet:3333@localhost")
        .name(test_name.to_str().unwrap())
        .on_eom(eom_callback)
        .actions(Actions::ADD_HEADER)
        .run()
        .expect("milter execution failed");

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");
}

#[on_eom(eom_callback)]
fn handle_eom(ctx: Context<()>) -> milter::Result<Status> {
    ctx.api.add_header("Test-Name", "Test-Value")?;

    Ok(Status::Continue)
}
