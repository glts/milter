conn = mt.connect("inet:3333@localhost")
assert(conn, "could not open connection")

local err = mt.eom(conn)
assert(err == nil, err)
assert(mt.getreply(conn) == SMFIR_CONTINUE)

assert(mt.eom_check(conn, MT_HDRADD, "Test-Name", "Test-Value"))

local err = mt.disconnect(conn)
assert(err == nil, err)
