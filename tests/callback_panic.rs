mod common;

use milter::*;
use std::sync::atomic::{AtomicUsize, Ordering};

#[test]
fn callback_panic() {
    let test_name = common::test_name(file!());
    let miltertest = common::spawn_miltertest_runner(file!());

    match Milter::new("inet:3335@localhost")
        .name(test_name.to_str().unwrap())
        .on_mail(mail_callback)
        .run()
    {
        Err(Error::CallbackPanic) => (),
        _ => panic!("expected panic did not occur"),
    }

    let exit_code = miltertest.join().expect("panic in miltertest runner");
    assert!(exit_code.success(), "miltertest returned error exit code");
}

static MAIL_COUNT: AtomicUsize = AtomicUsize::new(1);

#[on_mail(mail_callback)]
fn handle_mail(_: Context<()>, smtp_args: Vec<&str>) -> Status {
    match MAIL_COUNT.fetch_add(1, Ordering::Relaxed) {
        1 => assert_eq!(smtp_args[0], "from1@example.com"),
        2 => panic!("panic expected"),
        _ => unreachable!(),
    }

    Status::Continue
}
